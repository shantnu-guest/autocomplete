Source: autocomplete
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Benjamin Mesing <ben@debian.org>
Build-Depends: debhelper (>= 10), javahelper (>= 0.4), maven-repo-helper, ant
Build-Depends-Indep: default-jdk, default-jdk-doc, librsyntaxtextarea-java (>= 2.5), librsyntaxtextarea-java-doc  (>= 2.5)
Standards-Version: 4.1.1
Vcs-Git: https://anonscm.debian.org/git/pkg-java/autocomplete.git
Vcs-Browser: https://anonscm.debian.org/git/pkg-java/autocomplete.git
Homepage: https://github.com/bobbylight/AutoComplete

Package: libautocomplete-java
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Suggests: libautocomplete-java-doc
Description: Java library for auto-completion in text component
 AutoComplete is a library allowing you to add IDE-like auto-completion
 (aka "code completion" or "Intellisense") to any Swing JTextComponent.
 Special integration is added for RSyntaxTextArea.
 .
 It features:
  * Drop-down completion choice list
  * Optional companion "description" window, complete with full HTML
    support and navigable with hyperlinks
  * Optional parameter completion assistance for functions/methods,
    ala Eclipse and NetBeans
  * Completion information is typically specified in an XML file,
    but can even be dynamic.

Package: libautocomplete-java-doc
Architecture: all
Section: doc
Depends: ${java:Depends}, ${misc:Depends}
Recommends: default-jdk-doc, librsyntaxtextarea-java-doc
Suggests: libautocomplete-java
Description: Java library for auto-completion in text component (documentation)
 AutoComplete is a library allowing you to add IDE-like auto-completion
 (aka "code completion" or "Intellisense") to any Swing JTextComponent.
 Special integration is added for RSyntaxTextArea.
 .
 It features:
  * Drop-down completion choice list
  * Optional companion "description" window, complete with full HTML
    support and navigable with hyperlinks
  * Optional parameter completion assistance for functions/methods,
    ala Eclipse and NetBeans
  * Completion information is typically specified in an XML file,
    but can even be dynamic.
 .
 This package contains the API documentation of libautocomplete-java.
